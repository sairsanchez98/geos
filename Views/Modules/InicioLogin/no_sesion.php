<div class="tab-content mt-sm-60 mt-30">
                        <div class="tab-pane fade show active" role="tabpanel">
                            <div class="container">
                                <div class="hk-row">
                                    <div class="col-lg-4">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        <div class="text-capitalize font-weight-500 text-dark">Entrada 1</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                        <!-- resultados -->
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        <div class="text-capitalize font-weight-500 text-dark">Entrada 2</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                        <!-- resultados -->
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        <div class="text-capitalize font-weight-500 text-dark">Entrada 3</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                        <!-- resultados -->
                                            </div>
                                        </div>  
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>	