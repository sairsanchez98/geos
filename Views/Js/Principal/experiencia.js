var Formacion = new Vue({
    el: "#formacion",
    data:{

    },
    methods: {
        // CARGAR LA FORMACIÓN DEL USUARIO LOGUEADO
        Get_experiencia_user : function (){
            var get_ = SERVERURL+"Controllers/CtrlExperiencia.php?cargar_experiencia_user&a_nombre_propio";
            axios.get(SERVERURL+"Controllers/CtrlExperiencia.php?cargar_experiencia_user&a_nombre_propio")
            .then(function(response){
                console.log(response);
            });

            var tabla = $('#datable_experiencia').DataTable( {
                "ajax" : get_,
                "responsive": true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 0, "desc" ]],
                "columns": [
                    { "data": 2 },
                    { "data": 3 },
                    { "data": 4 },
                    { "data": 5 },
                    { "data": 6 },
                    { "data": 10 }
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Cargando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "(Vacío) Aún no se ha registrado la experiencia laboral",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();
        }


    },
    mounted() {
        this.Get_experiencia_user();
    },
})